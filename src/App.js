import React from 'react'

import Header from './components/Header/index'
import Content from './components/PageContent/index'

import './App.css'
function App() {

	const title = 'AGUARDE, POIS SUA EXPERIÊNCIA ESTÁ SENDO DESENVOLVIDA!'
	const text = 'A plataforma Three é uma plataforma de jogos totalmente brasileira. Sua experiência ao abrir um jogo não será mais a mesma.'

  	return (
    	<div>

			<Header/>
			<Content title={title} text={text}/>
			
    	</div>
  	);
}

export default App;
