import React, { useEffect, useState } from 'react'
// import {useHistory, useLocation, Link} from "react-router-dom"

import image from '../../assets/icon/3Three-B.png'

import './styles.css';

// import Callout from '../../Callout';

function HeaderLink({ onClick, title, Icon }) {
    return (
        <div
            className={'label-container'}
            onClick={onClick}
            id={title+'-button'}
        >
            <label className='label-title'>{title}</label>
        </div>
    )
}

export default function Header(){
    const [{ width }, setDimension] = useState({ width: window.innerWidth, height: window.innerHeight });
    const [dropMenuOpen, setDropMenuOpen] = useState(false);
    const [curCallout, setCurCallout] = useState(0);
    // const history = useHistory();
    // const location = useLocation();

    function handleInfo(){
        // history.push('/about')
    }

    // function MenuItem({ title, Content, Icon }) {
    //     const [itemExpanded, setItemExpanded] = useState(false);
    //     return(
    //         <>
    //             <HeaderLink
    //                 // Icon={Icon}
    //                 title={title}
    //                 onClick={
    //                     title === 'Contato' ?
    //                     () => setItemExpanded(!itemExpanded)
    //                     :
    //                     () => handleInfo()
    //                 }
    //             />
    //             <div
    //                 id='content'
    //                 className={itemExpanded ? 'content' : 'content-hiden'}
    //             >
    //                 <Content />
    //             </div>
    //         </>
    //     );
    // }

    // function onClickLink(link) {
    //     if(curCallout === link) {
    //         setCurCallout(0); // Fecha o callout
    //     } else {
    //         setCurCallout(link);
    //     }
    // }

    const updateWindowDimensions = () => {
        setDimension({ width: window.innerWidth, height: window.innerHeight });
    }

    useEffect(() => {
        window.addEventListener('resize', updateWindowDimensions);
        return () => window.removeEventListener('resize', updateWindowDimensions);
    }, [])

    useEffect(() => {
        if(width > 540 && dropMenuOpen) {
            setDropMenuOpen(false);
        } else if(width < 540 && curCallout) {
            setCurCallout(0);
        }
    }, [width, dropMenuOpen, curCallout]);

    
    return (
        <header>
            <div id="geral">
                <div className="left-content">
                    <div id="boximg">
                        <img src={image} className="img_logo" alt="logo do aplicativo" id="img_logo" />
                    </div> 
                </div>
                <div className='right-content'>
                    {width > 540 ? // pc
                        <>
                            
                            <HeaderLink title='Início' onClick={() => handleInfo()}/>
                            <HeaderLink title='Plataforma' onClick={() => handleInfo()}/>
                            <HeaderLink title='Quem Somos?' onClick={() => handleInfo()}/>

                        </>
                    : // mobile
                        <div
                            id='menu-button'
                            className={ dropMenuOpen ?  'menu-button-active' : 'menu-button' }
                            onClick={() => setDropMenuOpen(!dropMenuOpen)}
                        >
                            {/* <MenuIcon className='menu-icon'/> */}
                        </div>
                    }
                </div>
            </div>
            <div
                id='menu'
                className= {dropMenuOpen ? 'menu-open' : 'menu-closed'}
            >
                {/* <MenuItem Content={Contact}  title='Contato'/> */}
                
                {/* <MenuItem Content={About} Icon={LoginIcon} title='Login'/> */}
                {/* <HeaderLink title='Login' Icon={LoginIcon} onClick={() => handleLogin()}/> */}

            </div>
            {/* {curCallout ?
                <Callout onClickOutSide={() => setCurCallout(0)}>
                    {
                        {
                            1: <Contact/>,
                            2: <About/>,
                        }[curCallout]
                    }
                </Callout>
            : null } */}
        </header>
    )
}
