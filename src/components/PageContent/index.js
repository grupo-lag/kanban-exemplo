import React from 'react'

import Footer from '../Footer/index'
import './styles.css'

const PageContent = ({title, text}) =>{
    return(
        <>
            <div id='content-main-component'>
                
                <div className='content-title'>
                    <h2>{title}</h2>
                </div>

                <div className='content-text'>
                    <p>{text}</p>
                </div>

                <div className='button-text-container'>
                    <button className='button-menu-subscribe'>INSCREVER-SE</button>
                    <p className='button-text'> Increva-se para receber atualizações e ser notificado quando a plataforma estiver pronta!</p>
                </div>
                
                
            </div>

            <br/><br/><br/><br/>

            <Footer/>

            <br/><br/><br/>
        </>  
    )
}

export default PageContent;