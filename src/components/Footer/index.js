import React from 'react'

import { ReactComponent as Twitter } from '../../assets/icon/Footer/twitter.svg'
import { ReactComponent as Facebook } from '../../assets/icon/Footer/facebook.svg'
import { ReactComponent as Instagram } from '../../assets/icon/Footer/instagram.svg'
import LAG from '../../assets/icon/Footer/GRUPO-LAG_B.png'
import './styles.css'

const SocialMedia = ({Icon, text}) =>{
    return(
        <div className='socialMedia'>
            <Icon className='socialMedia-icon'/>
            <label className='socialMedia-text'>{text}</label>
        </div>
    )
}

const Footer = () =>{
    return (
        <Footer>
            <div className='footer'>
                <div className='socialMedia-container'>
                    <SocialMedia Icon = {Twitter} />
                    <SocialMedia Icon = {Facebook} />
                    <SocialMedia Icon = {Instagram} />
                </div>
                <img src ={LAG} alt='lag' className='footer-lag-image'/>

            </div>
        </Footer>
    )
}
export default Footer